Yet Another Entity Iterator

# Usage

```php
/** @var iterable $ids */
$ids = [1, 2, 3];

/** @var \Drupal\another_entity_iterator\EntityIterator\EntityIteratorFactoryInterface $iteratorFactory */
$iteratorFactory = \Drupal::service('another_entity_iterator.factory');
foreach ($iteratorFactory->get(entityType: \Drupal\entity_test\Entity\EntityTest::class, entityIds: $ids) as $entity) {
  assert($entity instanceof \Drupal\entity_test\Entity\EntityTest);
}
```

Normally, you'd use this project with controllers or services via autowiring:

```php
use Drupal\entity_test\Entity\EntityTest;
use Drupal\another_entity_iterator\EntityIterator\EntityIteratorFactoryInterface;

class MyService {

  public function __construct(
    private readonly EntityIteratorFactoryInterface $iteratorFactory,
  ) {
  }

  public function handleEntities(): void {
    foreach ($this->iteratorFactory->get(entityType: EntityTest::class, entityIds: [1, 2, 3]) as $entity) {
      assert($entity instanceof EntityTest);
    }
  }

}
```

# Optional configuration

## Chunk size

By default entities are loaded in batches of 50 entities.

To override, use the following setting:

```php
# settings.php
$settings['entity_update_batch_size'] = 12345;
```

# License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
