<?php

declare(strict_types=1);

namespace Drupal\Tests\another_entity_iterator\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\KernelTests\KernelTestBase;

/**
 * Chunked iterator test.
 *
 * @coversDefaultClass \Drupal\another_entity_iterator\EntityIterator\ChunkedIterator
 * @covers \Drupal\another_entity_iterator\EntityIterator\EntityIteratorFactory
 */
final class ChunkedIteratorTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'another_entity_iterator',
    'entity_test',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('entity_test');
  }

  /**
   * Tests chunked iterator.
   *
   * @dataProvider providerChunkedIterator
   */
  public function testChunkedIterator(bool $iteratorOrArray): void {
    $entities = array_map(fn (int $i): EntityTest => $this->createEntityTest(), range(1, 10));

    // This array doesn't have meaningful keys.
    $ids = array_map(fn (EntityTest $entity): int => (int) $entity->id(), $entities);
    $expectedIds = $ids;
    if ($iteratorOrArray) {
      // Turn $ids into a useless iterable thing.
      $ids = ((function () use ($ids) {
        yield from $ids;
      })());
    }

    /** @var \Drupal\another_entity_iterator\EntityIterator\EntityIteratorFactory $iteratorFactory */
    $iteratorFactory = \Drupal::service('another_entity_iterator.factory');
    $iterator = $iteratorFactory
      // Give it a non evenly divisible (10/3) number so both the yields in
      // ChunkedIterator::getIterator are covered.
      ->get(EntityTest::class, $ids, 3);

    $result = iterator_to_array($iterator);
    static::assertCount(10, $result);

    // The keys here are now the entity IDs.
    $resultIds = array_map(fn (EntityTest $entity) => $entity->id(), $result);
    static::assertEquals($expectedIds, array_values($resultIds));
    static::assertEquals($expectedIds, array_keys($resultIds));
  }

  /**
   * Data provider for testChunkedIterator.
   */
  public static function providerChunkedIterator(): \Generator {
    yield 'iterator' => [TRUE];
    yield 'array' => [FALSE];
  }

  /**
   * Create a test entity.
   */
  private function createEntityTest(): EntityTest {
    $entity = EntityTest::create();
    $entity->name->value = $this->randomMachineName();
    $entity->save();
    return $entity;
  }

}
