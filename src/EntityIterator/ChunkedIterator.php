<?php

declare(strict_types=1);

namespace Drupal\another_entity_iterator\EntityIterator;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Provides an Iterator class for dealing with large amounts of entities.
 *
 * Without loading them all into memory at once.
 *
 * @template TValue of \Drupal\Core\Entity\EntityInterface
 */
final readonly class ChunkedIterator implements \IteratorAggregate, \Countable {

  /**
   * Constructs an entity iterator object.
   */
  public function __construct(
    private EntityStorageInterface $entityStorage,
    private MemoryCacheInterface $memoryCache,
    private iterable $entityIds,
    private int $chunkSize = 50,
  ) {
  }

  /**
   * Implements \Countable::count().
   */
  public function count(): int {
    return count($this->entityIds);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return \Traversable<TValue>
   */
  public function getIterator(): \Traversable {
    $entityIds = is_array($this->entityIds)
      ? array_chunk(array_values($this->entityIds), $this->chunkSize)
      : ((function (): \Generator {
        $chunk = [];
        foreach ($this->entityIds as $id) {
          $chunk[] = $id;
          if (count($chunk) === $this->chunkSize) {
            yield $chunk;
            $chunk = [];
          }
        }
        yield $chunk;
      })());

    /** @var iterable<string|int> $idChunk */
    foreach ($entityIds as $idChunk) {
      // Never load all entities if chunk is empty.
      if (count($idChunk) === 0) {
        continue;
      }

      yield from $this->entityStorage->loadMultiple($idChunk);
      // We clear all memory cache as we want to remove all referenced entities
      // as well, like for example the owner of an entity.
      $this->memoryCache->deleteAll();
    }
  }

}
