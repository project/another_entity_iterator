<?php

declare(strict_types=1);

namespace Drupal\another_entity_iterator\EntityIterator;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Site\Settings;

/**
 * Factory for creating entity iterators.
 */
final readonly class EntityIteratorFactory implements EntityIteratorFactoryInterface {

  /**
   * Creates a new EntityIteratorFactory.
   *
   * @internal
   */
  public function __construct(
    private EntityTypeManagerInterface $entityTypeManager,
    private MemoryCacheInterface $memoryCache,
    private Settings $settings,
    private EntityTypeRepositoryInterface $entityTypeRepository,
  ) {
  }

  /**
   * Creates an iterator for loading entities in a memory efficient fashion.
   *
   * @param class-string<T> $entityType
   *   An entity type class.
   * @param iterable<int|string> $entityIds
   *   An iterable of entity IDS.
   * @param positive-int|null $chunkSize
   *   The quantity of entities to load at a time, or NULL to use the default.
   *
   * @phpstan-return \Drupal\another_entity_iterator\EntityIterator\ChunkedIterator<T>
   * @template T of \Drupal\Core\Entity\EntityInterface
   */
  public function get(string $entityType, iterable $entityIds, ?int $chunkSize = NULL): ChunkedIterator {
    $entityTypeId = $this->entityTypeRepository->getEntityTypeFromClass($entityType);
    $chunkSize ??= $this->settings::getInstance()::get('entity_update_batch_size', 50);
    if (!is_int($chunkSize) || $chunkSize < 1) {
      throw new \InvalidArgumentException('Invalid chunk size.');
    }

    /** @var \Drupal\another_entity_iterator\EntityIterator\ChunkedIterator<T> */
    return new ChunkedIterator(
      $this->entityTypeManager->getStorage($entityTypeId),
      $this->memoryCache,
      entityIds: $entityIds,
      chunkSize: $chunkSize,
    );
  }

}
