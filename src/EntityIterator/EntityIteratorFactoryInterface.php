<?php

declare(strict_types=1);

namespace Drupal\another_entity_iterator\EntityIterator;

/**
 * Factory for creating entity iterators.
 */
interface EntityIteratorFactoryInterface {

  /**
   * Creates an iterator for loading entities in a memory efficient fashion.
   *
   * @param class-string<T> $entityType
   *   An entity type class.
   * @param iterable<int|string> $entityIds
   *   An iterable of entity IDS. Unlike Drupal core, an empty iterable will
   *   load nothing, not all entities.
   * @param positive-int|null $chunkSize
   *   The quantity of entities to load at a time, or NULL to use the default.
   *
   * @phpstan-return \Drupal\another_entity_iterator\EntityIterator\ChunkedIterator<T>
   * @template T of \Drupal\Core\Entity\EntityInterface
   */
  public function get(string $entityType, iterable $entityIds, ?int $chunkSize = NULL): ChunkedIterator;

}
